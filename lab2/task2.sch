<Qucs Schematic 0.0.18>
<Properties>
  <View=0,0,987,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=task2.dat>
  <DataDisplay=task2.dpl>
  <OpenDisplay=1>
  <Script=task2.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Diode D1 1 490 250 -26 -29 0 2 "1e-15 A" 0 "1" 0 "10 fF" 0 "0.5" 0 "0.7 V" 0 "0.5" 0 "0.0 fF" 0 "0.0" 0 "2.0" 0 "0.0 Ohm" 0 "0.0 ps" 0 "0" 0 "0.0" 0 "1.0" 0 "1.0" 0 "0" 0 "1 mA" 0 "26.85" 0 "3.0" 0 "1.11" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "26.85" 0 "1.0" 0 "normal" 0>
  <IProbe I_shem 1 610 250 -26 16 0 0>
  <VProbe U_in 1 310 120 28 -31 0 0>
  <GND * 1 300 380 0 0 0 0>
  <GND * 1 760 380 0 0 0 0>
  <GND * 1 320 140 0 0 0 0>
  <VProbe U_out 1 880 120 28 -31 0 0>
  <GND * 1 890 140 0 0 0 0>
  <Vac V1 1 300 350 18 -26 0 1 "100 V" 1 "5 kHz" 1 "0" 0 "0" 0>
  <GND * 1 670 380 0 0 0 0>
  <R R1 1 760 350 15 -26 0 1 "10 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <.TR TR1 1 130 570 0 61 0 0 "lin" 1 "0.2 ms" 1 "0.4 ms" 1 "100" 0 "Trapezoidal" 0 "2" 0 "1 ns" 0 "1e-16" 0 "150" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "26.85" 0 "1e-3" 0 "1e-6" 0 "1" 0 "CroutLU" 0 "no" 0 "yes" 0 "0" 0>
  <C C1 1 670 350 17 -26 0 1 "5 uF" 1 "" 0 "neutral" 0>
</Components>
<Wires>
  <520 250 580 250 "" 0 0 0 "">
  <760 250 760 320 "" 0 0 0 "">
  <640 250 670 250 "" 0 0 0 "">
  <300 250 460 250 "" 0 0 0 "">
  <300 250 300 320 "" 0 0 0 "">
  <300 140 300 250 "" 0 0 0 "">
  <760 250 870 250 "" 0 0 0 "">
  <870 140 870 250 "" 0 0 0 "">
  <670 250 760 250 "" 0 0 0 "">
  <670 250 670 320 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
