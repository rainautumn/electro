<Qucs Schematic 0.0.18>
<Properties>
  <View=0,-25,1407,844,1,0,0>
  <Grid=10,10,1>
  <DataSet=lab2.dat>
  <DataDisplay=lab2.dpl>
  <OpenDisplay=1>
  <Script=lab2.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Diode D1 1 440 220 -26 -29 0 2 "1e-15 A" 0 "1" 0 "10 fF" 0 "0.5" 0 "0.7 V" 0 "0.5" 0 "0.0 fF" 0 "0.0" 0 "2.0" 0 "0.0 Ohm" 0 "0.0 ps" 0 "0" 0 "0.0" 0 "1.0" 0 "1.0" 0 "0" 0 "1 mA" 0 "26.85" 0 "3.0" 0 "1.11" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "26.85" 0 "1.0" 0 "normal" 0>
  <IProbe I_shem 1 560 220 -26 16 0 0>
  <VProbe U_in 1 260 90 28 -31 0 0>
  <GND * 1 250 350 0 0 0 0>
  <GND * 1 710 350 0 0 0 0>
  <GND * 1 270 110 0 0 0 0>
  <VProbe U_out 1 830 90 28 -31 0 0>
  <GND * 1 840 110 0 0 0 0>
  <Vac V1 1 250 320 18 -26 0 1 "100 V" 1 "5 kHz" 1 "0" 0 "0" 0>
  <.DC DC1 1 250 540 0 38 0 0 "26.85" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "no" 0 "150" 0 "no" 0 "none" 0 "CroutLU" 0>
  <.AC AC1 1 250 620 0 38 0 0 "const" 1 "1 GHz" 0 "10 GHz" 0 "[5 kHz]" 1 "no" 0>
  <.TR TR1 1 80 540 0 61 0 0 "lin" 1 "0.2 ms" 1 "0.4 ms" 1 "100" 0 "Trapezoidal" 0 "2" 0 "1 ns" 0 "1e-16" 0 "150" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "26.85" 0 "1e-3" 0 "1e-6" 0 "1" 0 "CroutLU" 0 "no" 0 "yes" 0 "0" 0>
  <R R1 1 710 320 15 -26 0 1 "10 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
</Components>
<Wires>
  <470 220 530 220 "" 0 0 0 "">
  <710 220 710 290 "" 0 0 0 "">
  <590 220 710 220 "" 0 0 0 "">
  <250 220 410 220 "" 0 0 0 "">
  <250 220 250 290 "" 0 0 0 "">
  <250 110 250 220 "" 0 0 0 "">
  <710 220 820 220 "" 0 0 0 "">
  <820 110 820 220 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
