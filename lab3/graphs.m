## Copyright (C) 2016 rainautumn
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

Ib = [0 5 10 20 50 80];
Ib = Ib .* 10^-6;

Ube_0 = [0.005 0.46 0.48 0.5 0.53 0.54];
Ube_5 = [0.005 0.55 0.58 0.6 0.63 0.63];

hold on;

plot(Ube_0, Ib, 'o');
plot(Ube_5, Ib, 'o');

Ib = [5 10 20 50 80];
Ib = Ib .* 10^-6;
Ube_0 = [0.46 0.48 0.5 0.53 0.54];
Ube_5 = [0.55 0.58 0.6 0.63 0.63];

x = 0.5:0.0001:0.525;
plot(x, aprocsimator(Ube_0, Ib, 5, x));
x = 0.597:0.0001:0.62;
plot(x, aprocsimator(Ube_5, Ib, 5, x));
x = 0.47:0.0001:0.55;
plot(x, (0.0273962 * (0.5125) -0.0128425) * (x - 0.5125) + aprocsimator(Ube_0, Ib, 5, 0.5125))

%Uke = [0.5 1 2 5 10 15];
%Ik_Ib_20 = [1.9 1.9 1.9 2 2.1 2.1];
%Ik_Ib_40 = [4.5 4.5 4.7 5 5.4 5.9];
%Ik_Ib_60 = [ 7.9 8.1 8.5 9.3 10.6 11.7];
%Ik_Ib_80 = [10.5 10.7 11.0 12.1 14.2 18.8];
%
%
%plot(Uke, Ik_Ib_20, 'o');
%plot(Uke, Ik_Ib_40, 'o');
%plot(Uke, Ik_Ib_60, 'o');
%plot(Uke, Ik_Ib_80, 'o');
%
%x = 3:0.1:15;
%plot(x, aprocsimator(Uke, Ik_Ib_20, 6, x));
%plot(x, aprocsimator(Uke, Ik_Ib_40, 6, x));
%plot(x, aprocsimator(Uke, Ik_Ib_60, 6, x));
%plot(x, aprocsimator(Uke, Ik_Ib_80, 6, x));
%
%x = 0.1:0.1:17;
%plot(x, (-0.00912623 * 7.5 + 0.329055) * (x - 7.5) + aprocsimator(Uke, Ik_Ib_60, 6, 7.5));