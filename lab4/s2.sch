<Qucs Schematic 0.0.18>
<Properties>
  <View=0,0,1892,1436,0.683014,0,52>
  <Grid=10,10,1>
  <DataSet=s2.dat>
  <DataDisplay=s2.dpl>
  <OpenDisplay=1>
  <Script=s2.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <C C1 1 1170 500 -26 17 0 0 "1 pF" 1 "" 0 "neutral" 0>
  <GND * 1 1750 430 0 0 0 0>
  <VProbe out_U1 1 1300 420 28 -31 0 0>
  <GND * 1 1480 730 0 0 0 0>
  <GND * 1 1310 440 0 0 0 0>
  <R R1 1 1480 590 15 -26 0 1 "1 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <Vac V1 1 620 630 18 -26 0 1 "5 V" 1 "1 kHz" 0 "0" 0 "0" 0>
  <GND * 1 620 730 0 0 0 0>
  <R R2 1 700 570 -26 15 0 0 "100 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <GND * 1 960 730 0 0 0 0>
  <R R3 1 960 470 15 -26 0 1 "107 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <R R4 1 960 650 15 -26 0 1 "20 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <R R5 1 1100 440 15 -26 0 1 "50 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <Vdc V2 1 1750 280 18 -26 0 1 "12 V" 1>
  <IProbe out_I1 1 1100 330 16 -26 0 1>
  <_BJT T1 1 1060 570 8 -26 0 0 "npn" 1 "1e-16" 1 "1" 1 "1" 0 "0" 0 "0" 0 "0" 1 "0" 0 "0" 0 "1.5" 0 "0" 0 "2" 0 "100" 1 "1" 0 "0" 0 "0" 0 "0" 0 "0" 0 "0" 0 "0" 0 "0.75" 0 "0.33" 0 "0" 0 "0.75" 0 "0.33" 0 "1.0" 0 "0" 0 "0.75" 0 "0" 0 "0.5" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "0.0" 0 "26.85" 0 "0.0" 0 "1.0" 0 "1.0" 0 "0.0" 0 "1.0" 0 "1.0" 0 "0.0" 0 "0.0" 0 "3.0" 0 "1.11" 0 "26.85" 0 "1.0" 0>
  <C C2 1 780 570 -26 17 0 0 "2 uF" 1 "" 0 "neutral" 0>
  <IProbe in_I1 1 860 570 -26 16 0 0>
  <VProbe in_U1 1 860 660 28 -31 0 0>
  <GND * 1 870 730 0 0 0 0>
  <.TR TR1 1 780 320 0 60 0 0 "lin" 1 "2 ms" 1 "3 ms" 1 "80" 0 "Trapezoidal" 0 "2" 0 "1 ns" 0 "1e-16" 0 "150" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "26.85" 0 "1e-3" 0 "1e-6" 0 "1" 0 "CroutLU" 0 "no" 0 "yes" 0 "0" 0>
  <GND * 1 1060 730 0 0 0 0>
</Components>
<Wires>
  <1480 620 1480 730 "" 0 0 0 "">
  <1480 500 1480 560 "" 0 0 0 "">
  <1200 500 1290 500 "" 0 0 0 "">
  <1290 500 1480 500 "" 0 0 0 "">
  <1290 440 1290 500 "" 0 0 0 "">
  <620 660 620 730 "" 0 0 0 "">
  <620 570 620 600 "" 0 0 0 "">
  <620 570 670 570 "" 0 0 0 "">
  <960 500 960 570 "" 0 0 0 "">
  <960 670 960 680 "" 0 0 0 "">
  <960 680 960 730 "" 0 0 0 "">
  <1100 500 1140 500 "" 0 0 0 "">
  <1100 470 1100 500 "" 0 0 0 "">
  <1750 310 1750 430 "" 0 0 0 "">
  <960 250 960 440 "" 0 0 0 "">
  <960 250 1100 250 "" 0 0 0 "">
  <1100 250 1750 250 "" 0 0 0 "">
  <1100 250 1100 300 "" 0 0 0 "">
  <1100 360 1100 410 "" 0 0 0 "">
  <1060 500 1100 500 "" 0 0 0 "">
  <960 570 960 620 "" 0 0 0 "">
  <960 570 1030 570 "" 0 0 0 "">
  <1060 500 1060 540 "" 0 0 0 "">
  <890 570 960 570 "" 0 0 0 "">
  <730 570 750 570 "" 0 0 0 "">
  <870 680 870 730 "" 0 0 0 "">
  <850 680 850 700 "" 0 0 0 "">
  <820 700 850 700 "" 0 0 0 "">
  <810 570 820 570 "" 0 0 0 "">
  <820 570 830 570 "" 0 0 0 "">
  <820 570 820 700 "" 0 0 0 "">
  <1060 600 1060 730 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 732 1350 1058 229 3 #c0c0c0 1 00 1 0 0.2 1 1 -0.1 0.5 1.1 1 -0.1 0.5 1.1 315 0 225 "" "" "">
	<"in_U1.Vt" #0000ff 0 3 0 0 0>
	<"out_U1.Vt" #ff0000 0 3 0 0 1>
  </Rect>
  <Rect 730 1060 1052 241 3 #c0c0c0 1 00 1 0.002 5e-05 0.003 1 -0.267039 0.1 0.0669103 1 -1 0.5 1 315 0 225 "" "" "">
	<"out_I1.It" #0000ff 0 3 0 0 1>
	<"in_I1.It" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
</Paintings>
