## Copyright (C) 2016 rainautumn
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} aprocsimator (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: rainautumn <rainautumn@FreeBSD>
## Created: 2016-04-10

function [aprocsimator] = aprocsimator (x, y, n, arg);
b = ones(0,5);
b(1) = sum(y);
b(2) = sum(y.*x);
b(3) = sum(y.*x.^2);
b(4) = sum(y.*x.^3);
b(5) = sum(y.*x.^4);

M = [n, sum(x), sum(x.^2), sum(x.^3), sum(x.^4);
     sum(x), sum(x.^2), sum(x.^3), sum(x.^4), sum(x.^5); 
     sum(x.^2), sum(x.^3), sum(x.^4), sum(x.^5), sum(x.^5);
     sum(x.^3), sum(x.^4), sum(x.^5), sum(x.^6), sum(x.^7); 
     sum(x.^4), sum(x.^5), sum(x.^6), sum(x.^7), sum(x.^8)];
         
a = inv(M) * b';


aprocsimator = a(1) + arg * a(2) + arg.^2 * a(3) + arg.^3 * a(4) + arg.^4 * a(5);
endfunction
