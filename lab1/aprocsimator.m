## Copyright (C) 2016 rainautumn
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} aprocsimator (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: rainautumn <rainautumn@FreeBSD>
## Created: 2016-04-10

function [aprocsimator] = aprocsimator (x, y, n, arg);
b = ones(0,3);
b(1) = sum(y);
b(2) = sum(y.*x);
b(3) = sum(y.*x.^2);

M = [n, sum(x), sum(x.^2);
     sum(x), sum(x.^2), sum(x.^3);  
     sum(x.^2), sum(x.^3), sum(x.^4);];    
a = inv(M) * b';


aprocsimator = a(1) + arg * a(2) + arg.^2 * a(3);
endfunction
